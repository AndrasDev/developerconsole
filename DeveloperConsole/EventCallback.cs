﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{
    public class EventCallback
    {
        public delegate void Callback();

        Callback handler;

        public void Invoke()
        {
            if (handler == null)
                return;

            handler();
        }

        public void Subscribe(Callback _callback)
        {
            if (handler == null)
            {
                handler = _callback;
                return;
            }

            handler += _callback;
        }
    }

    public class EventCallback<T>
    {
        public delegate void Callback(T newValue);

        Callback handler;

        public void Invoke(T _newValue)
        {
            if (handler == null)
                return;

            handler(_newValue);
        }

        public void Subscribe(Callback _callback)
        {
            if (handler == null)
            {
                handler = _callback;
                return;
            }

            handler += _callback;
        }
    }
}
