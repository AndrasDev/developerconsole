﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Andras.DeveloperConsole.DefaultCommands;


namespace Andras.DeveloperConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            DeveloperConsole.OnPrint.Subscribe((string message) =>
            {
                Console.WriteLine(message);
            });

            StringCommand name = new StringCommand("name");
            name.CurrentValue = "hello";

            IntCommand money = new IntCommand("money");
            money.SetMin(0);

            Vector3 position = new Vector3();

            Vector3Command cmdPosition = new Vector3Command("position");
            cmdPosition.X.OnChange.Subscribe((float _newValue) => { position.x = _newValue; });
            cmdPosition.Y.OnChange.Subscribe((float _newValue) => { position.y = _newValue; });
            cmdPosition.Z.OnChange.Subscribe((float _newValue) => { position.z = _newValue; });

            cmdPosition.Y.SetMin(0);


            FunctionCommand getPosition = new FunctionCommand("getPos");
            getPosition.OnCall.Subscribe(() =>
            {
                Console.WriteLine(position.ToString());
            });

            DeveloperConsole.AddCommand(cmdPosition.Generate());

            DeveloperConsole.AddCommand(getPosition);

            DeveloperConsole.AddCommand(name);
            DeveloperConsole.AddCommand(money);

            HandleInput();
        }

        static void HandleInput()
        {
            //DeveloperConsole.PrintAllCommands();

            try
            {
                DeveloperConsole.RunCommand(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }

            HandleInput();
        }
    }
}
