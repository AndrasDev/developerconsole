﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{
    public class AutoComplete
    {
        public string name;
        public string type;
        public string currentValue;

        public AutoComplete(string _name, string _type, string _currentValue)
        {
            name = _name;
            type = _type;
            currentValue = _currentValue;
        }

        public string GetDropdownText()
        {
            return name + " : " + type;
        }

        public string GetSelectText()
        {
            return name + " " + currentValue;
        }
    }
}
