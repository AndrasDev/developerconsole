﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Andras.DeveloperConsole.DefaultCommands;

namespace Andras.DeveloperConsole
{
    class Vector3Command : PropertyCommand
    {
        public FloatCommand X = new FloatCommand("x");
        public FloatCommand Y = new FloatCommand("y");
        public FloatCommand Z = new FloatCommand("z");

        public Vector3Command(string _name) : base(_name)
        {

        }

        public override List<ConsoleCommand> ToList()
        {
            List<ConsoleCommand> properties = new List<ConsoleCommand>();

            properties.Add(X);
            properties.Add(Y);
            properties.Add(Z);

            return properties;
        }

    }
}
