﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{
    public class MinMax
    {
        public static MinMax NoLimit
        {
            get { return new MinMax(); }
        }
    }

    public abstract class MinMax<T>
    {
        protected bool shouldUse = false;
        protected T value;

        public MinMax()
        {
            shouldUse = false;
        }

        public MinMax(T _value)
        {
            shouldUse = true;
            value = _value;
        }

        public virtual void DontUse()
        {
            shouldUse = false;
        }

        public virtual bool ShouldUse()
        {
            return shouldUse;
        }

        public virtual T Value()
        {
            return value;
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }

    public class Max<T> : MinMax<T>
    {
        Max() : base() { }
        Max(T _value) : base(_value) { }

        public static implicit operator Max<T>(T _value)
        {
            return new Max<T>(_value);
        }

        public static implicit operator Max<T>(MinMax _value)
        {
            return new Max<T>();
        }
    }

    public class Min<T> : MinMax<T>
    {
        Min() : base() { }
        Min(T _value) : base(_value) { }

        public static implicit operator Min<T>(T _value)
        {
            return new Min<T>(_value);
        }

        public static implicit operator Min<T>(MinMax _value)
        {
            return new Min<T>();
        }

        public override string ToString()
        {
            return Value() + " : " + ShouldUse();
        }
    }
}
