﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{
    public enum ComputeResult { Success, Fail, NoValue };

    public abstract class ConsoleCommand
    {
        public ConsoleCommand(string _namn)
        {
            Name = _namn;
        }

        public string Name;

        public abstract ComputeResult Compute(string _line, out string _message);
        public virtual bool Is(string _name)
        {
            _name = _name.Split(' ')[0];
            return _name.Equals(Name.ToLower());
        }
        public abstract string Print();
        public abstract string Type();
    }

    public abstract class ConsoleCommand<T> : ConsoleCommand
    {

        public ConsoleCommand(string _namn) : base(_namn)
        {
            OnChange = new EventCallback<T>();
        }

        public T CurrentValue;
        public EventCallback<T> OnChange;

        public abstract T Parse(string _line);
        public bool TryParse(string _line, out T _result, out string _message)
        {
            try
            {
                _result = Parse(_line);
                _message = "";
                return true;
            }
            catch (Exception e)
            {
                _message = e.Message;

                _result = Activator.CreateInstance<T>();
                return false;
            }
        }

        public T GetValue()
        {
            return CurrentValue;
        }

        public virtual void SetValue(T _newValue)
        {
            CurrentValue = _newValue;
            OnChange.Invoke(_newValue);
        }

        public override ComputeResult Compute(string _line, out string _messsage)
        {
            _messsage = "";

            if (String.IsNullOrEmpty(_line))
                return ComputeResult.NoValue;

            string[] args = _line.Split(' ');

            if (args.Length < 2)
                return ComputeResult.NoValue;

            if (args[0] == Name)
            {
                string values = "";

                for (int i = 1; i < args.Length; i++)
                    values += args[i] + " ";

                T result;

                if (TryParse(values, out result, out _messsage))
                {
                    SetValue(result);
                    return ComputeResult.Success;
                }
            }
            _messsage = "Invalid Value";
            return ComputeResult.Fail;
        }

        public abstract override string ToString();
    }
}
