﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole.DefaultCommands
{
    public class IntCommand : MinMaxCommand<int>
    {
        public IntCommand(string _name) : base(_name)
        {

        }

        public override bool GreaterThan(int _right, int _left)
        {
            return _right > _left;
        }

        public override bool LessThan(int _right, int _left)
        {
            return _right < _left;
        }

        public override int Parse(string _line)
        {
            return int.Parse(_line);
        }

        public override string Print()
        {
            return ToString();
        }

        public override string ToString()
        {
            return CurrentValue.ToString();
        }

        public override string Type()
        {
            return "int";
        }
    }
}
