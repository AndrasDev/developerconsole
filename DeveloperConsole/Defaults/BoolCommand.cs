﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole.DefaultCommands
{
    public class BoolCommand : ConsoleCommand<bool>
    {
        public BoolCommand(string _name) : base(_name){}

        public override bool Parse(string _line)
        {
            _line = _line.Trim().ToLower();

            if (_line == "true")
                return true;

            if (_line == "false")
                return false;

            if (_line == "1")
                return true;

            if (_line == "0")
                return false;

            throw new Exception("Invalid Value");
        }

        public override string Print()
        {
            return ToString();
        }

        public override string ToString()
        {
            return CurrentValue.ToString();
        }

        public override string Type()
        {
            return "boolean";
        }
    }
}
