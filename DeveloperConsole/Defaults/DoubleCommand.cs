﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole.DefaultCommands
{
    public class DoubleCommand : MinMaxCommand<double>
    {
        public DoubleCommand(string _name) : base(_name){}

        public override bool GreaterThan(double _right, double _left)
        {
            return _right > _left;
        }

        public override bool LessThan(double _right, double _left)
        {
            return _right < _left;
        }

        public override double Parse(string _line)
        {
            return double.Parse(_line);
        }

        public override string Print()
        {
            return ToString();
        }

        public override string ToString()
        {
            return CurrentValue.ToString();
        }

        public override string Type()
        {
            return "double";
        }
    }
}
