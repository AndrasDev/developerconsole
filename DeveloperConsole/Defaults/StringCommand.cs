﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole.DefaultCommands
{
    public class StringCommand : ConsoleCommand<string>
    {
        public Min<int> Min = MinMax.NoLimit;
        public Max<int> Max = MinMax.NoLimit;

        public void SetMin(Min<int> _min)
        {
            Min = _min;
        }

        public void SetMax(Max<int> _max)
        {
            Max = _max;
        }

        public StringCommand(string _name) : base(_name)
        {

        }

        public bool GreaterThan(string _right, int _left)
        {
            return _right.Length > _left;
        }

        public bool LessThan(string _right, int _left)
        {
            return _right.Length < _left;
        }

        public override string Parse(string _value)
        {
            return _value.Trim();
        }

        public override string Print()
        {
            return ToString();
        }

        public override string ToString()
        {
            return CurrentValue;
        }

        public override void SetValue(string _newValue)
        {
            if (Max.ShouldUse())
                if (GreaterThan(_newValue, Max.Value()))
                    throw new Exception("Length for " + Name + " is too high");

            if (Min.ShouldUse())
                if (LessThan(_newValue, Min.Value()))
                    throw new Exception("Length for " + Name + " is too low");

            base.SetValue(_newValue);
        }

        public override string Type()
        {
            return "string";
        }
    }
}
