﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole.DefaultCommands
{
    public class FloatCommand : MinMaxCommand<float>
    {
        public FloatCommand(string _name) : base(_name)
        {

        }

        public override bool GreaterThan(float _right, float _left)
        {
            return _right > _left;
        }

        public override bool LessThan(float _right, float _left)
        {
            return _right < _left;
        }

        public override float Parse(string _line)
        {
            return float.Parse(_line);
        }

        public override string Print()
        {
            return ToString();
        }

        public override string ToString()
        {
            return CurrentValue.ToString();
        }

        public override string Type()
        {
            return "float";
        }
    }
}
