﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{
    class FunctionCommand : ConsoleCommand
    {
        
        public EventCallback OnCall;

        public FunctionCommand(string _name) : base(_name)
        {
            OnCall = new EventCallback();
        }

        public override ComputeResult Compute(string _line, out string _messsage)
        {
            _messsage = "";

            if (String.IsNullOrEmpty(_line))
                return ComputeResult.NoValue;

            OnCall.Invoke();
            return ComputeResult.Success;
        }

        public override string Print()
        {
            return "function";
        }

        public override string Type()
        {
            return "function";
        }

        public override string ToString()
        {
            return "called";
        }
    }


}
