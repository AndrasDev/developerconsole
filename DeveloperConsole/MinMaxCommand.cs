﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{
    public abstract class MinMaxCommand<T> : ConsoleCommand<T>
    {

        public MinMaxCommand(string _name) : base(_name)
        {

        }

        public Min<T> Min = MinMax.NoLimit;
        public Max<T> Max = MinMax.NoLimit;

        public void SetMin(Min<T> _min)
        {
            Min = _min;
        }

        public void SetMax(Max<T> _max)
        {
            Max = _max;
        }

        public abstract bool LessThan(T _right, T _left);
        public abstract bool GreaterThan(T _right, T _left);

        public override ComputeResult Compute(string _line, out string _message)
        {
            _message = "";
            if (String.IsNullOrEmpty(_line))
                return ComputeResult.NoValue;

            string[] args = _line.Split(' ');

            if (args.Length < 2)
                return ComputeResult.NoValue;

            if (args[0] == Name)
            {
                string values = "";

                for (int i = 1; i < args.Length; i++)
                    values += args[i] + " ";

                T result;
                
                if (TryParse(values, out result, out _message))
                {
                    SetValue(result);
                    return ComputeResult.Success;
                }
            }
            _message = "Invalid Value";
            return ComputeResult.Fail;
        }

        public override void SetValue(T _newValue)
        {
            if (Max.ShouldUse())
                if (GreaterThan(_newValue, Max.Value()))
                    throw new Exception("New value for " + Name + " is too high");

            if (Min.ShouldUse())
                if (LessThan(_newValue, Min.Value()))
                    throw new Exception("New value for " + Name + " is too low");

            base.SetValue(_newValue);
        }
    }
}
