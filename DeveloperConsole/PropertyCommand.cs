﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{

    public abstract class PropertyCommand
    {
        public string Name;
        char Separator;

        public PropertyCommand(string _name, char _separator = '_')
        {
            Name = _name;
            Separator = _separator;
        }

        public List<ConsoleCommand> Generate()
        {
            List<ConsoleCommand> _values = ToList();

            for (int i = 0; i < _values.Count; i++)
            {
                _values[i].Name = Name + Separator + _values[i].Name;
            }

            return _values;
        }

        public abstract List<ConsoleCommand> ToList();
    }
}
