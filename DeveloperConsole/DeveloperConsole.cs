﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Andras.DeveloperConsole
{
    static class DeveloperConsole
    {
        static List<ConsoleCommand> commands = new List<ConsoleCommand>();

        public static ConsoleCommand LatestChanged = null;

        public static void AddCommand(ConsoleCommand cmd)
        {
            commands.Add(cmd);
        }

        public static void AddCommand(List<ConsoleCommand> cmds)
        {
            foreach (var cmd in cmds)
            {
                commands.Add(cmd);
            }
        }


        public static List<AutoComplete> GetAutoCompletes(string _currentText)
        {
            List<AutoComplete> completes = new List<AutoComplete>();

            foreach (var item in commands)
            {
                if (item.Name.StartsWith(_currentText))
                {
                    completes.Add(new AutoComplete(item.Name, item.Type(), item.Print()));
                }
            }

            return completes;
        }

        public static void PrintAllCommands()
        {
            foreach (var item in commands)
            {
                Console.WriteLine(item.Name + " : " + item.Print());
            }
        }

        public static void RunCommand(string _line)
        {
            _line = _line.Trim();

            if (String.IsNullOrEmpty(_line))
                return;

            foreach (var cmd in commands)
                if (cmd.Is(_line.ToLower()))
                {
                    string message = "";
                    switch (cmd.Compute(_line, out message))
                    {
                        case ComputeResult.Success:
                            LatestChanged = cmd;
                            Print(cmd.Name + ": " + cmd.ToString());
                            return;
                        case ComputeResult.Fail:
                            PrintError(message);
                            return;
                        case ComputeResult.NoValue:
                            Print(cmd.Name + ": " + cmd.ToString());
                            return;
                        default:
                            break;
                    }
                }

            PrintError("Invalid Command");
        }

        public static EventCallback<string> OnPrint { get; } = new EventCallback<string>();

        public static void Print(string message)
        {
            OnPrint.Invoke(message);
        }

        public static EventCallback<string> OnPrintError { get; } = new EventCallback<string>();

        public static void PrintError(string message)
        {
            OnPrintError.Invoke(message);
        }
    }
}
