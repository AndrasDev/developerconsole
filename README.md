# Developer Console

# Egen Datatyp

## Med MaxMin värde 
```C#
public class DoubleCommand : MinMaxCommand<double>
{
    public DoubleCommand(string _name) : base(_name){}

    public override bool GreaterThan(double _right, double _left)
    {
        return _right > _left;
    }

    public override bool LessThan(double _right, double _left)
    {
        return _right < _left;
    }

    public override double Parse(string _line)
    {
        return double.Parse(_line);
    }

    public override string Print()
    {
        return ToString();
    }

    public override string ToString()
    {
        return CurrentValue.ToString();
    }
}
```

## Utan MaxMin värde 
```C#
public class BoolCommand : ConsoleCommand<bool>
{
    public BoolCommand(string _name) : base(_name){}

    public override bool Parse(string _line)
    {
        _line = _line.Trim().ToLower();

        if (_line == "true")
            return true;

        if (_line == "false")
            return false;

        if (_line == "1")
            return true;

        if (_line == "0")
            return false;

        throw new Exception("Cant Parse");
    }

    public override string Print()
    {
        return ToString();
    }

    public override string ToString()
    {
        return CurrentValue.ToString();
    }
}
```

## Användning
```C#
DoubleCommand money = new DoubleCommand("money");
money.SetMin(0);

BoolCommand isMale = new BoolCommand("ismale");

DeveloperConsole.AddCommand(money);
DeveloperConsole.AddCommand(isMale);
```

# Egen Class
## Vector3
```C#
public class Vector3
{
    public float x, y, z;

    public Vector3(float _x, float _y, float _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    public Vector3(float _x, float _y)
    {
        x = _x;
        y = _y;
        z = 0;
    }

    public Vector3()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    public override string ToString()
    {
        return String.Format("({0}, {1}, {2})", x, y, z);
    }
}
```

## Vector3 Command
```C#
class Vector3Command : PropertyCommand
{
    public FloatCommand X = new FloatCommand("x");
    public FloatCommand Y = new FloatCommand("y");
    public FloatCommand Z = new FloatCommand("z");

    public Vector3Command(string _name) : base(_name){}

    public override List<ConsoleCommand> ToList()
    {
        List<ConsoleCommand> properties = new List<ConsoleCommand>();

        properties.Add(X);
        properties.Add(Y);
        properties.Add(Z);

        return properties;
    }
}
```

## Användning
```C#
Vector3 position = new Vector3();

Vector3Command cmdPosition = new Vector3Command("position");
cmdPosition.X.OnChange.Subscribe((float _newValue) => { position.x = _newValue; });
cmdPosition.Y.OnChange.Subscribe((float _newValue) => { position.y = _newValue; });
cmdPosition.Z.OnChange.Subscribe((float _newValue) => { position.z = _newValue; });

cmdPosition.Y.SetMin(0);

DeveloperConsole.AddCommand(cmdPosition.Generate());
```

# Kolla Input
```C#
DeveloperConsole.RunCommand("key value"); //ändrar värdet om det är giltigt

DeveloperConsole.RunCommand("key"); //printar ut värdet
```